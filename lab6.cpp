/**************************
 *Noah Klimkowski
 *CPSC 1021-01
 *nklimko@g.clemson.edu
 *Nushrat
 **************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[])
{
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
  employee arr[10];
  for(int i = 0; i < 10; i++)
  {
    cout<<"Last name: ";
    cin>>arr[i].lastName;
    cout<<"First name: ";
    cin>>arr[i].firstName;
    cout<<"Birth year: ";
    cin>>arr[i].birthYear;
    cout<<"Hourly Wage: ";
    cin>>arr[i].hourlyWage;
    cout<<"\n";
  }


  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
    random_shuffle(arr, arr+10, myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
    employee sorted[5];
    for(int i = 0; i < 5; i++)
    {
      sorted[i] = arr[i];
    }

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
    employee temp;

    //bubble sort implemented
    for(int i = 0; i < 5; i++)
    {
      for(int j = i+1; j < 5; j++)
      {
        //name order returns true if in order, swaps if false
        if(!name_order(sorted[i], sorted[j]))
        {
          temp = sorted[i];
          sorted[i] = sorted[j];
          sorted[j] = temp;
        }
      }      
    }


    /*Now print the array below */
    for(int i = 0; i < 5; i++)
    {
      cout << sorted[i].lastName << ", " << sorted[i].firstName<<"\n";
      cout << sorted[i].birthYear << "\n"; 
      cout << sorted[i].hourlyWage <<"\n";
    }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs)
{
  // IMPLEMENT

  int length;
  int dif;
  bool leftFirst;
  bool lengthDif;
  bool contested = true;
  int i = 0;

  //checks for longest name to prevent error of accesing character beyond string
  //additionally sets return value in event of similar start names ie "Rob" vs "Robert"
  if(lhs.lastName.length()>rhs.lastName.length())
  {
    length = rhs.lastName.length();
    lengthDif = false;
  }
  else
  {
    length = lhs.lastName.length();
    lengthDif = true;
  }
  
  //condition is while there is no difference and shortest string hasnt been exhausted
  while(contested && i < length)
  {
    //subtracts ascii values of indexed letter
    dif = lhs.lastName[i] - rhs.lastName[i];

    //condition of same letter
    if(dif == 0)
    {
      i++;
      if(i == length)
      {
        return lengthDif;
      }  
    }
    //right before left
    else if(dif > 0)
    {
      contested = false;
      leftFirst = false;
    }
    //left before right
    else if(dif < 0)
    {
      contested = false;
      leftFirst = true;
    }
    //debug, theoratically unreachable
    else
    {
      cout<<"huge error";
    } 
  }
  //returns value set by dif comparisons
  return leftFirst;
}

